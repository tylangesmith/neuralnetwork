# Neural Network: Simple NN Structures

*Neural Network* is a simple python library for creating neural network structures. This library is designed to avoid all of the unnecessary bloat functionality that is usually packaged with other machine learning libraries.

*Network → Nodes → Connections → Activate*

## Usage

This library gives the flexibility to easily create neural network structures defining the network nodes and connections.

```python
# OR Gate NN example
from neuralnetwork import FeedForwardNeuralNetwork
from neuralnetwork import NodeTypes

# Create the network, in this case a feedforward network
network = FeedForwardNeuralNetwork()

# Add the network nodes defining the type of node, activation function and node bias
network.add_node('x1', NodeTypes.Input, ActivationFunctionTypes.Identity, 0.0)
network.add_node('x2', NodeTypes.Input, ActivationFunctionTypes.Identity, 0.0)
network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid, 0.0)

# Add the network connections defining the input node, output node and connection weight
network.add_connection('c1', 'x1', 'y1', 0)
network.add_connection('c2', 'x2', 'y1', 0)

# Now activate the network
network.activate([0.0, 0.0]) # Output: 0.0
network.activate([0.0, 1.0]) # Output: 1.0
network.activate([1.0, 0.0]) # Output: 1.0
network.activate([1.0, 1.0]) # Output: 1.0
```

## Authors

* Ty Lange-Smith - [Website](https://www.tylangesmith.com.au)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details