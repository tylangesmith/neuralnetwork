import unittest
from neuralnetwork import FeedForwardNeuralNetwork
from neuralnetwork import NodeTypes
from neuralnetwork import ActivationFunctionTypes

class Test_FeedForwardNeuralNetwork(unittest.TestCase):

    def test_add_node_not_already_added(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        # Assert
        self.assertTrue(len(network.nodes.keys()) == 3)
        self.assertTrue(len(network.graph.nodes) == 3)

    def test_add_node_already_added(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        exception = None
        try:
            network.add_node('x1', NodeTypes.Input)
            network.add_node('x1', NodeTypes.Input)
        except Exception as e:
            exception = e

        # Assert
        self.assertTrue(len(network.nodes.keys()) == 1)
        self.assertTrue(len(network.graph.nodes) == 1)
        self.assertIsNotNone(exception)

    def test_remove_node_that_exists(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        network.remove_node('x1')

        # Assert
        self.assertTrue(len(network.nodes.keys()) == 2)
        self.assertTrue(len(network.graph.nodes) == 2)

    def test_remove_node_that_does_not_exists(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        network.remove_node('x3')

        # Assert
        self.assertTrue(len(network.nodes.keys()) == 3)
        self.assertTrue(len(network.graph.nodes) == 3)

    def test_add_connection_not_already_added(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        network.add_connection('c1', 'x1', 'y1', 0.0)
        network.add_connection('c2', 'x2', 'y1', 0.0)

        # Assert
        self.assertTrue(len(network.connections.keys()) == 2)
        self.assertTrue(len(network.graph.edges) == 2)

    def test_add_connection_already_added(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        exception = None
        try:
            network.add_connection('c1', 'x1', 'y1', 0.0)
            network.add_connection('c1', 'x1', 'y1', 0.0)
        except Exception as e:
            exception = e

        # Assert
        self.assertTrue(len(network.connections.keys()) == 1)
        self.assertTrue(len(network.graph.edges) == 1)
        self.assertIsNotNone(exception)
    
    def test_add_connection_nodes_not_exists(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        exception = None
        try:
            network.add_connection('c1', 'x1', 'y1', 0.0)
            network.add_connection('c2', 'x1', 'y2', 0.0)
        except Exception as e:
            exception = e

        # Assert
        self.assertTrue(len(network.connections.keys()) == 1)
        self.assertTrue(len(network.graph.edges) == 1)
        self.assertIsNotNone(exception)

    def test_add_connection_causes_cycle(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        exception = None
        try:
            network.add_connection('c1', 'x1', 'y1', 0.0)
            network.add_connection('c2', 'y1', 'x1', 0.0)
        except Exception as e:
            exception = e

        # Assert
        self.assertTrue(len(network.connections.keys()) == 1)
        self.assertTrue(len(network.graph.edges) == 1)
        self.assertIsNotNone(exception)

    def test_remove_connection_that_exists(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        network.add_connection('c1', 'x1', 'y1', 0.0)
        network.add_connection('c2', 'x2', 'y1', 0.0)
        network.remove_connection('c1')

        # Assert
        self.assertTrue(len(network.connections.keys()) == 1)
        self.assertTrue(len(network.graph.edges) == 1)

    def test_remove_connection_that_does_not_exists(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        network.add_connection('c1', 'x1', 'y1', 0.0)
        network.add_connection('c2', 'x2', 'y1', 0.0)
        network.remove_connection('c3')

        # Assert
        self.assertTrue(len(network.connections.keys()) == 2)
        self.assertTrue(len(network.graph.edges) == 2)

    def test_does_connection_cause_cycle_with_cylce_connection(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        network.add_connection('c1', 'x1', 'y1', 0.0)
        network.add_connection('c2', 'x2', 'y1', 0.0)

        causesCycle = network.does_connection_cause_cycle('y1', 'x1')

        # Assert
        self.assertTrue(causesCycle)

    def test_does_connection_cause_cycle_without_cylce_connection(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Sigmoid)

        network.add_connection('c1', 'x1', 'y1', 0.0)
        network.add_connection('c2', 'x2', 'y1', 0.0)

        causesCycle = network.does_connection_cause_cycle('x1', 'y1')

        # Assert
        self.assertFalse(causesCycle)

    def test_activate(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Identity)

        network.add_connection('c1', 'x1', 'y1', 1.0)
        network.add_connection('c2', 'x2', 'y1', 1.0)

        outputs = network.activate([1.0, 1.0])

        # Assert
        self.assertTrue(len(outputs) == 1)
        self.assertEquals(outputs[0].value, 2.0)

    def test_activate_incorrect_inputs(self):
        # Arrange
        network = FeedForwardNeuralNetwork()

        # Act
        network.add_node('x1', NodeTypes.Input)
        network.add_node('x2', NodeTypes.Input)
        network.add_node('y1', NodeTypes.Output, ActivationFunctionTypes.Identity)

        network.add_connection('c1', 'x1', 'y1', 1.0)
        network.add_connection('c2', 'x2', 'y1', 1.0)

        exception = None
        try:
            network.activate([1.0, 1.0, 1.0])
        except Exception as e:
            exception = e

        # Assert
        self.assertIsNotNone(exception)

if __name__ == '__main__':
    unittest.main()