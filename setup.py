from setuptools import setup

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='neuralnetwork',
   version='0.1',
   license="MIT",
   long_description=long_description,
   description='A simple lightweight library to create and activate neuralnetworks.',
   author='Ty Lange-Smith',
   author_email='tylangesmith@gmail.com',
   packages=['neuralnetwork'],
   install_requires=['networkx']
)