import uuid

class Connection:

    def __init__(self, id, inputNode, outputNode, weight):
        self.id = id
        self.inputNode = inputNode
        self.outputNode = outputNode
        self.weight = weight

        outputNode.inputConnections[id] = self