import enum

class NodeTypes(enum.Enum):
    Input = 1,
    Hidden = 2,
    Output = 3