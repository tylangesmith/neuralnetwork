import enum

class ActivationFunctionTypes(enum.Enum):
    Identity = 1,
    Sigmoid = 2,
    Tanh = 3,
    Step = 4