import uuid
from .activation_function import ActivationFunction

class Node:

    def __init__(self, id, nodeType, activationFunctionType, bias):
        self.id = id
        self.nodeType = nodeType
        self.activationFunction = ActivationFunction(activationFunctionType)
        self.bias = bias
        self.value = 0.0
        self.inputConnections = {}

    def add_connection(self, connection):
        self.inputConnections[connection.id] = connection
            
    def calculate_value(self):
        # Sum each input connection node by the input connection weight
        inputSum = self.bias
        for inputConnection in self.inputConnections.values():
            inputSum += (inputConnection.inputNode.value * inputConnection.weight)
        self.value = self.activationFunction.activate(inputSum)
        
    def reset(self):
        self.value = 0.0