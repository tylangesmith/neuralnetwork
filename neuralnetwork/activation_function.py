import math

from .activation_function_types import ActivationFunctionTypes

class ActivationFunction:

    def __init__(self, activationFunction):
        self.activate = self.identity

        if activationFunction == ActivationFunctionTypes.Identity:
            self.activate = self.identity

        if activationFunction == ActivationFunctionTypes.Sigmoid:
            self.activate = self.sigmoid

        if activationFunction == ActivationFunctionTypes.Tanh:
            self.activate = self.tanh

        if activationFunction == ActivationFunctionTypes.Step:
            self.activate = self.step

    def identity(self, x):
        return x

    def sigmoid(self, x):
        if x >= 0:
            z = math.exp(-x)
            return 1 / (1 + z)
        else:
            z = math.exp(x)
            return z / (1 + z)

    def tanh(self, x):
        return math.tanh(x)

    def step(self, x):
        if x >= 1:
            return 1
        else:
            return 0