import uuid
import networkx as nx

from .activation_function_types import ActivationFunctionTypes
from .node_types import NodeTypes
from .node import Node
from .connection import Connection

class FeedForwardNeuralNetwork:

    def __init__(self):
        self.id = str(uuid.uuid4())
        self.graph = nx.DiGraph()
        self.nodes = {}
        self.connections = {}

    def add_node(self, id, nodeType, activationFunctionType=ActivationFunctionTypes.Identity, bias=0.0):
        # First check if there is a node that already exists for this id
        if self.nodes.__contains__(id):
            raise Exception('This node id is already added to the network')

        node = Node(id, nodeType, activationFunctionType, bias)
        self.nodes[node.id] = node
        self.graph.add_node(node.id)
        
    def remove_node(self, id):
        if self.nodes.__contains__(id):
            del self.nodes[id]
            self.graph.remove_node(id)

    def add_connection(self, id, inputNodeId, outputNodeId, weight):
        # First lets ensure that the connection does not already exist
        if self.connections.__contains__(id):
            raise Exception('This connection id is already added to the network')
        
        # Now check that both input and output nodes exist
        if not self.nodes.__contains__(inputNodeId) or not self.nodes.__contains__(outputNodeId):
            raise Exception('The given input or output nodes do not exist')
            
        # Now check that the connection does not cause a cycle
        if self.does_connection_cause_cycle(inputNodeId, outputNodeId):
            raise Exception('This connection causes a cycle')

        # The connection should now be safe to add
        inputNode = self.nodes[inputNodeId]
        outputNode = self.nodes[outputNodeId]
        connection = Connection(id, inputNode, outputNode, weight)

        self.connections[connection.id] = connection
        self.graph.add_edge(inputNode.id, outputNode.id)

    def remove_connection(self, id):
        if self.connections.__contains__(id):
            connection = self.connections[id]
            inputNodeId = connection.inputNode.id
            outputNodeId = connection.outputNode.id
            del self.connections[id]
            self.graph.remove_edge(inputNodeId, outputNodeId)        

    def does_connection_cause_cycle(self, inputNodeId, outputNodeId):
        # Create a copy of the graph with the new connection
        tempGraph = self.graph.copy()
        tempGraph.add_edge(inputNodeId, outputNodeId)

        # Determine if it contains a cylce
        try:
            nx.algorithms.find_cycle(tempGraph)
        except nx.exception.NetworkXNoCycle:
            return False
        return True

    def activate(self, inputs):
        # First lets check if the inputs match the node inputs
        nodeInputs = [node for node in self.nodes.values() if node.nodeType == NodeTypes.Input]
        if len(inputs) != len(nodeInputs):
            raise Exception("Incorrect amount of inputs")

        # First reset all of the previous node values back to 0
        self.reset()

        # Ensure that we are of the correct topological sort order
        sortedNodes = list(nx.topological_sort(self.graph))

        for index, sortedNode in enumerate(sortedNodes):
            node = self.nodes[sortedNode]

            if node.nodeType == NodeTypes.Input:
                node.value = inputs[index]

            if node.nodeType == NodeTypes.Hidden or node.nodeType == NodeTypes.Output:
                node.calculate_value()

        return [outputNode for outputNode in self.nodes.values() if outputNode.nodeType == NodeTypes.Output]

    def reset(self):
        for node in self.nodes.values():
            node.reset()